const { spy } = require('sinon')
const { expect } = require('chai')
const { Readable } = require('streamx')

const { hyperInterfaceFactory, testnetFactory } = require('./fixtures')
const { asHex, asBuffer } = require('hexkey-utils')

describe('Test swarm-interface', function () {
  let hyperInterface, hyperInterface2
  let testnet, swarmInterface1, swarmInterface2

  this.beforeEach(async function () {
    hyperInterface = await hyperInterfaceFactory()
    hyperInterface2 = await hyperInterfaceFactory()

    testnet = await testnetFactory(
      hyperInterface.corestore,
      hyperInterface2.corestore
    )
    swarmInterface1 = testnet.swarmInterface1
    swarmInterface2 = testnet.swarmInterface2
  })

  this.afterEach(function () {
    // DEVNOTE: all async, but faster not to await
    // (particularly destroy takes a while)
    hyperInterface.close()
    hyperInterface2.close()
    testnet.destroy()
  })

  it('is possible to serve and read a remote hypercore', async function () {
    const serverCore = await hyperInterface.createCore('test core', {
      valueEncoding: 'utf-8'
    })
    await swarmInterface1.serveCore(serverCore.discoveryKey)
    await serverCore.append('Content')

    const clientCore = await hyperInterface2.readCore(
      serverCore.key, { valueEncoding: 'utf-8' }
    )
    await swarmInterface2.requestCore(clientCore.discoveryKey)

    const actual = await clientCore.get(0)
    expect(actual).to.eq('Content')
  })

  it('can serve multiple cores at once', async function () {
    const core = await hyperInterface.createCore('test core')
    const core2 = await hyperInterface.createCore('test core2')

    const discKeys = [asHex(core.discoveryKey), asHex(core2.discoveryKey)]

    await swarmInterface1.serveCores(discKeys)

    expect(swarmInterface1.replicatedDiscoveryKeys).to.deep.have.same.members(discKeys)
    expect(swarmInterface1.servedDiscoveryKeys).to.deep.have.same.members(discKeys)

    await core.append('block0')
    await core2.append('otherblock')

    const clientCore = await hyperInterface2.readCore(core.key, { valueEncoding: 'utf-8' })
    const clientCore2 = await hyperInterface2.readCore(core2.key, { valueEncoding: 'utf-8' })

    const promise1 = swarmInterface2.requestCore(clientCore.discoveryKey)
    const promise2 = swarmInterface2.requestCore(clientCore2.discoveryKey)
    await Promise.all([promise1, promise2])

    expect(await clientCore.get(0)).to.eq('block0')
    expect(await clientCore2.get(0)).to.eq('otherblock')
  })

  it('can request multiple cores at once', async function () {
    const core = await hyperInterface.createCore('test core')
    const core2 = await hyperInterface.createCore('test core2')

    const discKeys = [asHex(core.discoveryKey), asHex(core2.discoveryKey)]
    await swarmInterface1.requestCores(discKeys)

    expect(swarmInterface1.replicatedDiscoveryKeys)
      .to.deep.have.same.members(discKeys)

    expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(0)
  })

  it('Does not unserve a core when requesting it', async function () {
    // Note: this is probably not an issue which can happen
    // --rm test+functionality when certain
    const core = await hyperInterface.createCore('test core')
    await swarmInterface1.serveCore(core.discoveryKey)
    expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(1)

    await swarmInterface1.requestCore(core.discoveryKey)
    expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(1)
  })

  it('Can unserve a key', async function () {
    const core = await hyperInterface.createCore('test core')
    await swarmInterface1.serveCore(core.discoveryKey)
    expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(1)

    await swarmInterface1.unserveCore(core.discoveryKey)
    expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(0)
  })

  describe('multiple keys tests', function () {
    let core, core2, core3
    let key, key2, key3

    this.beforeEach(async function () {
      core = await hyperInterface.createCore('test core')
      core2 = await hyperInterface.createCore('test core2')
      core3 = await hyperInterface.createCore('test core3')
      key = core.discoveryKey
      key2 = core2.discoveryKey
      key3 = core3.discoveryKey
    })

    it('Can unserve multiple keys', async function () {
      await swarmInterface1.serveCores([key, key2, key3])
      expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(3)

      await swarmInterface1.unserveCores([key, key2])
      expect(swarmInterface1.servedDiscoveryKeys).to.deep.equal([asHex(key3)])
    })

    it('Can serve from streamx stream (async iter)', async function () {
      const stream = new Readable()
      stream.push(key)
      stream.push(key2)
      stream.push(null)

      await swarmInterface1.serveCores(stream)
      expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(2)
    })

    it('Can unserve from streamx stream (async iter)', async function () {
      await swarmInterface1.serveCores([key, key2, key3])
      expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(3)

      const stream = new Readable()
      stream.push(key)
      stream.push(key2)
      stream.push(key3)
      stream.push(null)

      await swarmInterface1.unserveCores([key, key2])
      expect(swarmInterface1.servedDiscoveryKeys).to.deep.equal([asHex(key3)])
    })

    it('Only joins cores it is not already serving on serveCores', async function () {
      await swarmInterface1.serveCores([key, key2])
      expect(swarmInterface1.servedDiscoveryKeys.length).to.equal(2)

      const joinSpy = spy(swarmInterface1.swarm, 'join')
      await swarmInterface1.serveCores([key, key3])
      expect(joinSpy.callCount).to.equal(1)
      expect(joinSpy.getCall(0).calledWith(asBuffer(key3))).to.equal(true)
    })
  })

  describe('Correctly returns replicated/served keys', async function () {
    let servedDiscoveryKey, requestedDiscoveryKey

    this.beforeEach(async function () {
      const serverCore = await hyperInterface.createCore('test core', {
        valueEncoding: 'utf-8'
      })
      servedDiscoveryKey = asHex(serverCore.discoveryKey)

      await swarmInterface1.serveCore(serverCore.discoveryKey)

      // Need not be an actual core for this test, so we read something random
      const requestedCore = await hyperInterface.readCore('a'.repeat(64))
      requestedDiscoveryKey = asHex(requestedCore.discoveryKey)

      await swarmInterface1.requestCore(requestedCore.discoveryKey)
    })

    it('contains all cores for replicatedKeys', function () {
      const actual = swarmInterface1.replicatedDiscoveryKeys
      const expected = [servedDiscoveryKey, requestedDiscoveryKey]

      expect(actual).to.have.members(expected)
    })

    it('contains only served cores for servedKeys', function () {
      const actual = swarmInterface1.servedDiscoveryKeys
      const expected = [servedDiscoveryKey]

      expect(actual).to.have.members(expected)
    })
  })
})
