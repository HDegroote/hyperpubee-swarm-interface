# Hyperpubee Swarm Interface

A small layer on top of hyperswarm to facilitate swarming the hypercores of a single corestore.

Discovery keys can be passed both as hexadecimal string or as buffer.

Note: still in alfa

## Install
`npm add hyperpubee-swarm-interface`

## API

```
const swarmInterface = new SwarmInterface(swarm, corestore)
```
Create a new swarm interface and setup replication (at corestore level).

### Properties
```
const replicatedKeys = swarmInterface.replicatedDiscoveryKeys
```
Get a list of all replicated keys (as client) in hex notation

```
const servedKeys = swarmInterface.servedDiscoveryKeys

```
Get a list of all served keys in hex notation

### Methods

```
await swarmInterface.serveCore (discoveryKey)
```
Serve the core with the given discovery key (join the swarm as both client and server)

```
await swarmInterface.serveCores (discoveryKeys)
```
Serve multiple cores (faster then adding 1-by-1).

discoveryKeys can be anything iterable or async iterable.

```
await swarmInterface.unserveCore (discoveryKey)
```
Stop announcing a core on the swarm.

Note that anyone who has set up replication with your corestore
will still be able to download that core from you, if they know
its public key.

```
await swarmInterface.unserveCores (discoveryKeys)
```
Stop announcing multiple cores

```
await swarmInterface.requestCore (discoveryKey)
```
Request the core with the given discovery key (join the swarm as client, but not as server)

```
await swarmInterface.requestCores (discoveryKeys)
```
Request multiple cores (faster than requesting 1-by-1)


```
const isClient = swarmInterface.hasDiscoveryKey(key)
```
Returns true if the discoveryKey is requested, false otherwise

```
const isServer = swarmInterface.servesDiscoveryKey(key)
```
Returns true if the discovery key is served, false otherwise
(in particular, returns false if joined only as client)

```
await swarmInterface.close()
```
Destroys the swarm and cleans up (does not close the corestore)
