const { asBuffer, asHex } = require('hexkey-utils')

class SwarmInterface {
  constructor (swarm, corestore) {
    this.swarm = swarm
    this.corestore = corestore

    this.setupReplication()
  }

  setupReplication () {
    this.swarm.on('connection', (socket) => {
      this.corestore.replicate(socket)
      socket.on('error', (err) => {
        console.error('socket error:', err.stack)
        socket.destroy()
      })
    })

    this.swarm.on('error', (e) => {
      console.error('swarm error:', e)
    })
  }

  _serveCore (discoveryKey) {
    this.swarm.join(asBuffer(discoveryKey), { server: true, client: true })
  }

  async serveCore (discoveryKey) {
    await this.serveCores([discoveryKey])
  }

  async serveCores (discoveryKeys) {
    const alreadyServed = new Set(this.servedDiscoveryKeys)

    let addedOne = false
    for await (const key of discoveryKeys) {
      if (!alreadyServed.has(asHex(key))) {
        addedOne = true
        this._serveCore(key)
      }
    }

    if (addedOne) await this.swarm.flush()
  }

  async unserveCore (discoveryKey) {
    await this.unserveCores([discoveryKey])
  }

  async unserveCores (discoveryKeys) {
    const promises = []
    for await (const key of discoveryKeys) {
      promises.push(this.swarm.leave(asBuffer(key)))
    }
    await Promise.all(promises)
  }

  async _requestCore (discoveryKey) {
    // If it's already served, we don't stop serving it (unsure if needed)
    const server = this.servesDiscoveryKey(discoveryKey)
    this.swarm.join(asBuffer(discoveryKey), { server, client: true })
  }

  async requestCore (discoveryKey) {
    await this.requestCores([discoveryKey])
  }

  async requestCores (discoveryKeys) {
    const alreadyReq = new Set(this.replicatedDiscoveryKeys)

    let addedOne = false
    for await (const key of discoveryKeys) {
      if (!alreadyReq.has(asHex(key))) {
        addedOne = true
        this._requestCore(key)
      }
    }

    if (addedOne) await this.swarm.flush()
  }

  get replicatedDiscoveryKeys () {
    const topicObjs = this.swarm.topics()

    const res = Array.from(topicObjs)
      .filter((topicObj) => topicObj.isClient)
      .map((topicObj) => asHex(topicObj.topic))

    return res
  }

  get servedDiscoveryKeys () {
    const topicObjs = this.swarm.topics()

    const res = Array.from(topicObjs)
      .filter((topicObj) => topicObj.isServer)
      .map((topicObj) => asHex(topicObj.topic))

    return res
  }

  hasDiscoveryKey (key) {
    return this.replicatedDiscoveryKeys.includes(asHex(key))
  }

  servesDiscoveryKey (key) {
    return this.servedDiscoveryKeys.includes(asHex(key))
  }

  async close () {
    for (const conn of this.swarm.connections) {
      conn.end()
    }
    await this.swarm.destroy()
    this.swarm.removeAllListeners()
  }
}

module.exports = SwarmInterface
